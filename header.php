<header id="header-3" class="header">
    <div class="main-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            <a class="navbar-brand" href="index.php"><img class="nav-logo" src="images/logo/logo.png" alt=""></a> </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="search-bar navbar-right">
                                <li><a href="#search"><i class="fa fa-search"></i></a></li>
                                <li id="search" class="search-form">
                                    <form class="header-search" action="#" method="post">
                                        <input type="search" name="search" placeholder="Type Here">
                                        <span class="src-close"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </form>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">

                                <li> <a href="index.php">Home</a> </li>
                                <li> <a href="service.php">Services</a> </li>
                                <li> <a href="about-us.php">About Us</a> </li>
                                <li> <a href="contact.php">Contact</a> </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                        <!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <div class="footer-widget">
                    <img class="nav-logo" src="images/logo/logo.png" alt="">
                    <div class="widget-content">
                        <p>Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text </p>
                        <ul class="address">
                            <li><i class="fa fa-map-marker"></i> <span>Shaba House(Ground Floor), House # 34, Room # 21, Road # 06, Gulshan-2, Dhaka - 1212</span></li>
                            <li><i class="fa fa-phone"></i> <span>(+880) 1912 480 444, <br> (+880) 1777 603 067</span></li>
                            <li><i class="fa fa-envelope"></i> <span>alamin24pvt.bd@gmail.com, support@alamin24securityservices.com</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widget-title">Usefull Links</h3>
                    <div class="widget-content">
                        <ul>
                            <li style="display: block;">
                                <div>
                                    <a href="terms-and-condition.php">Terms &amp; Conditions</a>
                                </div>
                            </li>
                            <li style="display: block;">
                                <div>
                                    <a href="about-us.php">About Us</a>
                                </div>
                            </li>
                            <li style="display: block;">
                                <div>
                                    <a href="contact.php">Contact Us</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widget-title">Newsletter</h3>
                    <div class="widget-content">
                        <p>Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text Companey Details Text</p>
                        <form method="post" action="#">
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                            <button class="btn btn-primary" type="submit" name="newsletter">Send</button>
                        </form>
                        <div class="footer-social-icon">
                            <h3 class="color-white">Find Us In</h3>
                            <ul class="social-icon">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-wifi"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Footer Bottom-->
<div id="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                Created with <i class="fa fa-heart" style="color:#fb3533" aria-hidden="true"></i> By <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a>
            </div>
        </div>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">

<?php require('head.php'); ?>
	
<body class="page-wrapper home-page-3">

<?php require('preloader.php'); ?>
    
<!--Header Section-->

<?php require('header.php'); ?>

<!--Slider Section-->

<?php require('slider.php') ?>

<!--Board Member Section-->

<?php require('board_member_section.php'); ?>

<!--Team Section-->

<?php require('team_section.php'); ?>

<!--Our Service Section-->

<?php require('our_services_section.php'); ?>


<!-- analitics Section -->
<?php require('analytics_section.php'); ?>

<!--Review Section-->
<?php require('review_section.php'); ?>

<!--Photo Gallery Section-->
<?php require('photo_gallery.php'); ?>

<!--Footer-->
<?php require('footer.php'); ?>

<!--JAVASCRIPT-->
<?php require('foot.php'); ?>

</body>

</html>
<section class="full-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title-area wow fadeIn" data-wow-delay="300ms" data-wow-duration="500ms">
                    <h2 class="section-title"><span class="title-tag">What Client Says</span>Reviews</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="testimonials-carousel">
                    <div class="item">
                        <div class="feedback">
                            <img class="avata" src="images/testimonial/1.jpg" alt="">
                            <p>Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews </p>
                            <div class="testimonial-person-detail">
                                <h4 class="thumb-title">Astian Flakelar</h4>
                                <span>CEO Gsm Group</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="feedback">
                            <img class="avata" src="images/testimonial/2.jpg" alt="">
                            <p>Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews </p>
                            <div class="testimonial-person-detail">
                                <h4 class="thumb-title">Hayden Dallachy</h4>
                                <span>Human resources</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="feedback">
                            <img class="avata" src="images/testimonial/1.jpg" alt="">
                            <p>Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews Some Reviews </p>
                            <div class="testimonial-person-detail">
                                <h4 class="thumb-title">Claudia Harker</h4>
                                <span>Corporate secretary</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
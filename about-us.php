	<!DOCTYPE html>
<html lang="en">

<?php require('head.php'); ?>

<body class="page-wrapper home-page-2">
    
<?php require('preloader.php'); ?>
		
<!--Header Section-->

<?php require('header.php'); ?>

    <!--Banner Section-->
	<section id="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-content">
						<h1 class="page-titile">About Us</h1>
						<ul class="banner-nav pull-right">
							<li><a href="index.php">Home</a></li>
							<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
							<li class="active"><a href="about-us.php">About Us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--Company Intro Section-->
	<section class="full-row overflow-hidden">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="500ms">
						<h3 class="inner-title">Company Intro</h3>
						<p>AL-AMIN 24 SECURITY SERVICES (PVT.) LTD. has rapidly developed in to household name as a provider of professional quality security services to the Dhaka local market/areas.</p>
						<p>Our headquarters are based in Gulshan, Dhaka as is our 24 hours control room which uses the latest technology to track, schedule and manage the day actives or the securities officers.</p>
					</div>
				</div>
				<!--<div class="col-md-6 col-sm-12">
					<div class="about-video wow fadeInRight" data-wow-delay="300ms" data-wow-duration="500ms">
						<a class="video-popup" href="https://vimeo.com/173404890" title="video popup"><span class="flaticon-play-button play"></span></a>
					</div>
				</div>-->
			</div>
		</div>
	</section>
	<!--Our Aim Section-->
	<section class="full-row bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="image-rotate margin-bottom-30 wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms">
						<div class="overflow-hidden">
							<a href="our-mission.html"><img src="images/our-aim/1.png" alt=""></a>
						</div>
						<a href="our-mission.html"><h4 class="thumb-title">Our Mission</h4></a>
						<p>We will endeavor to set our services apart from other security companies whose rates are high and whose delivery often falls short of expectation. We only recruit committed staff.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="image-rotate margin-bottom-30 wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms">
						<div class="overflow-hidden">
							<a href="our-vision.html"><img src="images/our-aim/2.png" alt=""></a>
						</div>
						<a href="our-vision.html"><h4 class="thumb-title">Our Vision</h4></a>
						<p>Our reputation has been developed on the basis of our attention in details, our rigorous staff training and vetting procedures and appreciation of our clients’ needs. Our approach is based around providing customer satisfaction and developing loyalty by ensuring that our services, systems and procedures work to everyone’s benefit.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="image-rotate margin-bottom-30 wow fadeInDown" data-wow-delay="300ms" data-wow-duration="500ms">
						<div class="overflow-hidden"><a href="service-values.html"><img src="images/our-aim/3.png" alt=""></a></div> 
						<a href="service-values.html"><h4 class="thumb-title">Values</h4></a>
						<p>Our client list is excessive as well as impressive and encompasses a wide range of Market, Industries, Finance, Construction, Local Authorities, Legal, Hotels, Resort, Restaurant, Fast Food & other Chain Shop and so on.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Our History Section-->
	<section class="full-row">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="history-detail margin-bottom-30 padding30">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="section-title-area-left">
									<h3 class="section-title">Company Activities</h3>
									<span class="sub-title">We will endeavor to set our services apart from other security companies whose rates are high and whose delivery often falls short of expectation. We only recruit committed staff.</span>
								</div>
							</div>
						</div>

                        <ul class="history-list">
                            <li>
                                <span>OPERATIVES</span>
                                <h5 class="thumb-title">AL-AMIN 24 SECURITY SERVICES (PVT.) LTD</h5>
                                <p>Will provide you with high caliber, highly trained and dedicated operatives, our customer must have the final decision on the selection and duties to be performed by our personal operation on their premises.</p>
                            </li>
                            <li>
                                <span>LECTURES</span>
                                <h5 class="thumb-title">AL-AMIN 24 SECURITY SERVICES (PVT.) LTD</h5>
                                <p>Can provide lecture on the correct security procedures to be performed by our personal operation on their duties.</p>
                            </li>
                            <li>
                                <span>SUPERVISING</span>
                                <h5 class="thumb-title">AL-AMIN 24 SECURITY SERVICES (PVT.) LTD</h5>
                                <p>Carries out a random check by a supervisor or manager to ensure that, our staff is fulfilling their duties.</p>
                            </li>
                            <li>
                                <span>IMPROVEMENT</span>
                                <h5 class="thumb-title">AL-AMIN 24 SECURITY SERVICES (PVT.) LTD</h5>
                                <p>Always endeavors to improve our services</p>
                            </li>
                            <li>
                                <span>SUGGESTIONS</span>
                                <h5 class="thumb-title">AL-AMIN 24 SECURITY SERVICES (PVT.) LTD</h5>
                                <p>Always invites new suggestions regarding improvement services from our honorable client.</p>
                            </li>
                        </ul>

					</div>
				</div>

                <div class="col-md-6 col-sm-12">
                    <div class="history-detail margin-bottom-30 padding30">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="section-title-area-left">
                                    <h3 class="section-title">SERVICE OFFERINGS</h3>
                                    <span class="sub-title">Our services are inclusive of</span>
                                </div>
                            </div>
                        </div>

                        <ul class="history-list">
                            <li>
                                <h5 class="thumb-title">Fully uniformed and equipped Static and Mobile Security Officers</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">Fully trained officers in Security and Health & Safety</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">24 hours central control center in constant communication with all officers</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">Electronic clocking</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">Portable two-way radio systems</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">We use to regular scheduled and unscheduled inspections</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">Portable two-way radio systems</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">TQM(Total Quality Management) Provide through Board of Directors Senior Managers instruction for the site</h5>
                            </li>
                            <li>
                                <h5 class="thumb-title">A regular visit of Account Managers to ensure client satisfaction is being maintained</h5>
                            </li>
                        </ul>

                    </div>
                </div>
			</div>
		</div>
	</section>

    <!--RECRUITMENT-->
    <section class="full-row overflow-hidden">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="history-detail margin-bottom-30 padding30">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="section-title-area-left">
                                    <h3 class="section-title">RECRUITMENT PROCESS</h3>
                                    <span class="sub-title">we recruit by -  </span>

                                    <p>On the basis of the Position/Services requirement of the organization the following steps have been following by our management team. There is an established requirement committee advertise in the daily news papers, leaflet distribution & the source of reliable information etc. Security the CV & Job application call interview cards for attending Primary selection, written test, Practical demonstration, Medical checkup, Physical fitness, Cross check certificate, Police verification of the final selected candidates. After their joining we arrange an orientation/training to the stuffs before starting their duties.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="history-detail margin-bottom-30 padding30">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="section-title-area-left">
                                    <h3 class="section-title">OUR DRESSES</h3>
                                    <span class="sub-title">Our dress code would be - </span>
                                </div>
                            </div>
                        </div>
                        <ul class="history-list">
                            <li>Color of shirt: Cofee Color</li>
                            <li>Color of pant: Black</li>
                            <li>Color of Cap : Cofee Color</li>
                            <li>Color of Boot: Black </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--OUR CLIENTS-->
    <section class="full-row overflow-hidden">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <div class="history-detail margin-bottom-30 padding30">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="section-title-area-left">
                                    <h3 class="section-title">CLIENTS</h3>
                                    <span class="sub-title">Here are all our clients -  </span>

                                    <div class="according_area">
                                        <div class="according_title">Metropolitan Shopping Plaza, Gulshan-2, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-35, House-34, Gulshan-2, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-42, House-24, Gulshan-2, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-24, House-04, Gulshan-1, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-23, House-78, Gulshan-1, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-123, House-21, Khursid Garden, Gulshan-2, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-18, House-28, Block-A, Banani, Dhaka-1213</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road-04, House-2/3, Banani, Dhaka-1213</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Sham Bazar, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">29 Naya Palton (Kids TV), Palton, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Chamelibagh, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Altaf Nagor, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Azad Garden, Block-J, Baridhara, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Meghna Car Wash Nadda, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">New Megpai Hotel (Basundhara), Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Somirudidn Kacha Bazar, Basundhara, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Diabetics Hospital, Netrokona</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Mirpur 60 Feet Road, Alimuddin School, Agargaon</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road # 90, House # 8/B, Gulshan-2, Dhaka-1212</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Hatabunano Garments, Khandoker Barir More, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Purobi Project, Nurerchala, Badda, Dhaka</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>
                                    <div class="according_area">
                                        <div class="according_title">Road # 18, House # 15, Block-A, Banani, Dhaka-1213</div>
                                        <div class="according_details">
                                            <p>Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. Some Details about this client. </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


	<?php require('analytics_section.php'); ?>
	
	<!--Footer Section-->
    <?php require('footer.php'); ?>

    <?php require('foot.php'); ?>
</body>

</html>
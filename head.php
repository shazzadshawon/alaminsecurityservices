<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="bodyguard, cyber security, guard, office security, privet security, security, security company, security guard, security service">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Al-Amin Security Services</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/default-animation.css" rel="stylesheet">
    <link href="fonts/flaticon/flaticon.css" rel="stylesheet">
    <link href="css/range-slider.css" rel="stylesheet">
    <link rel="stylesheet" href="css/color.css" id="color-change">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/loader.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon.ico">
</head>
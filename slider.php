<div id="slider">
    <div class="slider-item">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active"> <img src="images/slider/1.JPG" alt="..."></div>
                <div class="item"> <img src="images/slider/3.JPG" alt="..."></div>
                <div class="item"> <img src="images/slider/4.JPG" alt="..."></div>
            </div>
        </div>
    </div>
</div>
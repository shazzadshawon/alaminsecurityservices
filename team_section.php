<section class="full-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title-area wow fadeIn" data-wow-delay="300ms" data-wow-duration="500ms">
                    <h2 class="section-title"><span class="title-tag">Our Other</span>Members</h2>
                </div>
            </div>
            <div class="welcome-item wow fadeInDown" data-wow-delay="300ms" data-wow-duration="500ms" style="text-align: center">
                <img src="images/our-team/no.png" style="width: 200px;" alt="">
                <a href="our-vision.html"><h4 class="thumb-title">Mrs. Razba Bagom </h4></a>
                <p>Exicutive Dirrector</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="team-member full-row margin-bottom-30"> <img src="images/our-team/no.png" alt="" style="width: 100%;">
                    <div class="team-title padding-15 text-center">
                        <h6>Dr. Md. Elaish Hossain</h6>
                        <span class="designation">- Dirrector -</span> </div>
                    <div class="team-overlay">
                        <div class="team-title padding-15 text-center">
                            <a href="#">
                                <h6>Dr. Md. Elaish Hossain</h6>
                            </a> <span class="designation">- Dirrector -</span></div>
                        <div class="social-icon margin-top-30"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team-member full-row margin-bottom-30"> <img src="images/our-team/no.png" alt="" style="width: 100%;">
                    <div class="team-title padding-15 text-center">
                        <h6>Md. Mirazul Islam</h6>
                        <span class="designation">- Manager -</span> </div>
                    <div class="team-overlay">
                        <div class="team-title padding-15 text-center">
                            <a href="#">
                                <h6>Md. Mirazul Islam</h6>
                            </a> <span class="designation">- Manager -</span>
                        </div>
                        <div class="social-icon margin-top-30"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team-member full-row margin-bottom-30"> <img src="images/our-team/no.png" alt="" style="width: 100%;">
                    <div class="team-title padding-15 text-center">
                        <h6>Md. Nazrul Islam</h6>
                        <span class="designation">- Marketing Manager -</span> </div>
                    <div class="team-overlay">
                        <div class="team-title padding-15 text-center">
                            <a href="#">
                                <h6>Md. Nazrul Islam</h6>
                            </a> <span class="designation">- Marketing Manager -</span></div>
                        <div class="social-icon margin-top-30"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team-member full-row margin-bottom-30"> <img src="images/our-team/no.png" alt="" style="width: 100%;">
                    <div class="team-title padding-15 text-center">
                        <h6>Md. Delower Hossain</h6>
                        <span class="designation">- Supervisor -</span> </div>
                    <div class="team-overlay">
                        <div class="team-title padding-15 text-center">
                            <a href="#">
                                <h6>Md. Delower Hossain</h6>
                            </a> <span class="designation">- Supervisor -</span>
                        </div>
                        <div class="social-icon margin-top-30"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!DOCTYPE html>
<html lang="en">

<?php require ('head.php'); ?>

<body class="page-wrapper home-page-2">
    
<?php require('preloader.php'); ?>

<!--Header Section-->

<?php require('header.php'); ?>

<!--Banner Section-->
<section id="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner-content">
					<h1 class="page-titile">Service Details</h1>
					<ul class="banner-nav pull-right">
						<li><a href="index-2.html">Home</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
						<li><a href="index-2.html">Service</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
						<li class="active"><a href="#">Service Details Name Here</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- General Support -->
<section class="full-row">
	<div class="container">
		<div class="row">

            <?php require('service_details_sidebar.php'); ?>

            <?php require('service_details_content.php'); ?>

		</div>
	</div>
</section>

<?php require('analytics_section.php'); ?>

<!--Footer Section-->
<?php require('footer.php'); ?>

<?php require('foot.php'); ?>

</body>

</html>
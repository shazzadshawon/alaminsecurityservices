<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/YouTubePopUp.jquery.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/mixitup.js"></script>
<script src="js/validate.js"></script>
<script src="js/wow.js"></script>
<script src="js/custom.js"></script>

<!-- use for map style -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPZ-Erd-14Vf2AoPW2Pzlxssf6-2R3PPs&amp;callback=initMap"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmyQ2zzyHHTQK2k9SqfHbcdhHiS9cPlbo&amp;callback=initMap"></script>
<script src="js/map.scripts.js"></script>
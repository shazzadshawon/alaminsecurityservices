<section class="full-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title-area wow fadeIn" data-wow-delay="300ms" data-wow-duration="500ms">
                    <h2 class="section-title"><span class="title-tag">Our Honerable</span>Board Members</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="welcome-item wow fadeInDown" data-wow-delay="300ms" data-wow-duration="500ms" style="text-align: center">
                    <img src="images/board/cm.jpg" alt="" style=" width: 300px; border-radius: 50%;">
                    <h4 class="thumb-title">Warrant Officer Sheikh Md. Shorab Hossain (Retd.) </h4>
                    <p>Chairman</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="welcome-item wow fadeInDown" data-wow-delay="300ms" data-wow-duration="500ms" style="text-align: center">
                    <img src="images/board/md.jpg" alt="" style=" width: 300px; border-radius: 50%;">
                    <h4 class="thumb-title">Md. Akram Hossain (Julash)</h4>
                    <p>Managing Director</p>
                </div>
            </div>
        </div>
    </div>
</section>
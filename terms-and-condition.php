<!DOCTYPE html>
<html lang="en">

<?php require('head.php'); ?>

<body class="page-wrapper home-page-2">
    
<?php require('preloader.php'); ?>
		
<!--Header Section-->

<?php require('header.php'); ?>

<!--Banner Section-->
<section id="banner">
	<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="banner-content">
					<h1 class="page-titile">Terms and Condition</h1>
					<ul class="banner-nav pull-right">
						<li><a href="index-2.html">Home</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
                        <li><a href="index-2.html">Pages</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
						<li class="active"><a href="#">Terms and Condition</a></li>
					</ul>
				</div>
			</div>
        </div>
    </div>
</section>

<!--Condition Page Start-->
<section class="full-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="row">
					<div class="col-md-12">
                        <div class="condition_details">
                            <h3 class="inner-title">User Agreement</h3>
                            <div class="time margin-bottom-15">Effective From: February 23, 2017</div>

                            <ol>
                                <li style="list-style: decimal;">Contract Period : 1 Year / 2 Years / 3 Years.</li>
                                <li style="list-style: decimal;">Salary/allowance and other staff related costs (Such as uniform and other supplies, compensation for bonus, yearly increment, insurance, medical leave, annual leave, sick leave and any other benefits applicable to staff costs.)</li>
                                <li style="list-style: decimal;">2nd Party will pay  50% Eid Bonus of basic salary.</li>
                                <li style="list-style: decimal;">Overhead costs (Such as office expenses monitoring and patrolling, depreciation cost for vehicles and equipment costs etc.)</li>
                                <li style="list-style: decimal;">Payment schedule: Any payment by company Account pay Cheque in cash handing by inform Managing Director.</li>
                                <li style="list-style: decimal;">Payment schedule: Any payment by company Account pay Cheque in cash handing by inform Managing Director.</li>
                            </ol>

                            <p>In this circumstance, we would welcome the opportunity to meet and discuss your security needs. We shall also be pleased to conduct a free of charge security survey of your premises and submit a report together with our proposal.</p>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Condition Page End-->

<!--Footer Section-->
<?php require('footer.php'); ?>

<?php require('foot.php'); ?>

</body>

</html>
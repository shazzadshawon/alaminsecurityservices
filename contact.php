<!DOCTYPE html>
<html lang="en">

<?php require('head.php'); ?>

<body class="page-wrapper home-page-2">
    
<?php require('preloader.php'); ?>
		
<!--Header Section-->

<?php require('header.php'); ?>

<!--Banner Section-->
<section id="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner-content">
					<h1 class="page-titile">Contact us</h1>
					<ul class="banner-nav pull-right">
						<li><a href="index.php">Home</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
						<li class="active"><a href="contact.php">Contact us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="full-row">
	<div class="container">
		<div class="row flex-box">
			<div class="col-md-8 col-sm-6">
				<div class="contact-us bg-gray padding30">
					<h3 class="inner-title">Get In Touch</h3>
					<span class="margin-bottom-15">Dont hesitate to contact us in any concern</span>
					<form id="contact-form" class="contact_message" action="#" method="post">
						<div class="row">
							<div class="form-group col-md-6 col-sm-6">
								<input class="form-control" id="name" name="firstname" placeholder="Name" type="text">
							</div>
							<div class="form-group col-md-6 col-sm-6">
								<input class="form-control" id="email" name="email" placeholder="Email Address" type="text">
							</div>
							<div class="form-group col-md-12 col-sm-12">
								<input class="form-control" id="subject" name="subject" placeholder="Subject" type="text">
							</div>
							<div class="form-group col-md-12 col-sm-12">
								<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
							</div>
							<div class="form-group col-md-12 col-sm-6">
								<input class="btn btn-primary margin-top-20" id="send" value="send message" type="submit">
							</div>
							<div class="col-md-12">
								<div class="error-handel">
									<div id="success">Your email sent Successfully, Thank you.</div>
									<div id="error"> Error occurred while sending email. Please try again later.</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="contact-detail padding30 color-white bg-dark">
					<h3 class="inner-title color-white">Get In Touch</h3>
					<span class="sub-title color-white">Here you can find our contact details</span>
					<span class="color-default">Phone Number</span>
					<p>(+880) 1912 480 444</p>
					<p>(+880) 1777 603 067</p>
					<span class="color-default">E-Mail</span>
					<p>alamin24pvt.bd@gmail.com</p>
					<p>support@alamin24securityservices.com</p>
					<span class="color-default">Address</span>
					<p>Shaba House(Ground Floor), House # 34, Room # 21, Road # 06, Gulshan-2, Dhaka - 1212</p>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="full-row">
	<div class="container-fluid">
		<div class="row">
			<div id="map"></div>
		</div>
	</div>
</div> 
<!--Footer Section-->
<?php require('footer.php'); ?>

<?php require('foot.php'); ?>

</body>

</html>
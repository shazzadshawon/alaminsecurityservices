<section class="full-row background-1 overlay-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title-area wow fadeIn" data-wow-delay="300ms" data-wow-duration="500ms">
                    <h2 class="section-title color-white"><span class="title-tag">See Our Experience</span>Photo Gallery</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="photo-gallery">
                    <div class="item">
                        <div class="gallery-item"> <img src="images/gallery/gal/1.JPG" alt="" style="width: 100%;height: 300px;">
                            <div class="overlay traingle"><a href="images/gallery/1.png" class="img_view info">  <span class="flaticon-add plus"></span></a> </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="gallery-item"> <img src="images/gallery/gal/2.JPG" alt="" style="width: 100%;height: 300px;">
                            <div class="overlay traingle"><a  href="images/gallery/2.png" class="img_view info"> <span class="flaticon-add plus"></span></a> </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="gallery-item"> <img src="images/gallery/gal/3.JPG" alt="" style="width: 100%;height: 300px;">
                            <div class="overlay traingle"><a href="images/gallery/3.png" class="img_view info">  <span class="flaticon-add plus"></span></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!DOCTYPE html>
<html lang="en">

<?php require('head.php'); ?>

<body class="page-wrapper home-page-2">

<?php require('preloader.php'); ?>
		
<!--Header Section-->

<?php require('header.php'); ?>

<!--Banner Section-->
<section id="banner">
	<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="banner-content">
					<h1 class="page-titile">Service</h1>
					<ul class="banner-nav pull-right">
						<li><a href="index.php">Home</a></li>
						<li><a href="#"><span class="fa fa-angle-right"></span></a></li>
						<li class="active"><a href="service.php">Service</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Our Services -->
<section class="full-row">
	<div class="container">
		<div class="row">

            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">House/Apartment Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Bank Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Building Conostruction Site Security</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Car Park Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Office Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Hospital Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Embassy Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Indutrial Site Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Schools, College & University Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Hotel, Restaurant, Fast Food & Other Chain</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Uniform Guards</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">V.I.P Protection</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Parks Patrol</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">C.N.G / Fuel Filling Station</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Vehicle Showroom</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Video Intercoms</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Alarm System</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Fire Alarm System</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">I.D Card System</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">C.C. TV</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>

		</div>
	</div>
</section>

<?php require('analytics_section.php'); ?>

<!--Footer Section-->
<?php require('footer.php'); ?>

<?php require('foot.php'); ?>

</body>

</html>
<section class="bg-gray our-servic-3 overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title-area wow fadeIn" data-wow-delay="300ms" data-wow-duration="500ms">
                    <h2 class="section-title"><span class="title-tag">What We Offer</span>Our Services</h2>
                    <!--<span class="sub-title">Suspendisse ipsum ante risus fusce nisl, cum blandit. Taciti cras egestas semper tristique ultrices magnis ut est pellentesque.</span>--> </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">House/Apartment Secutity</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Bank/Banging Security</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Building Construction Site Security</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Car Park Security</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">Industrial Site Security</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item bg-white text-center wow fadeInDown" data-wow-delay="200ms" data-wow-duration="500ms" style="display: table">
                    <div class="service-caption">
                        <span><img src="images/serviceicons/image.png" style="width: 54px;"></span>
                        <a class="margin-top-20" href="service-details.php">
                            <h4 class="service-title">School, College, University</h4>
                        </a>
                        <a class="btn-link" href="service-details.php">Read More</a> </div>
                </div>
            </div>

        </div>
    </div>
</section>